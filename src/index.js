import {Play} from "./stages/play/";
var game = new Phaser.Game( 300, 500, Phaser.AUTO, '' );
game.state.add( 'Play', Play(game) );
game.state.start( 'Play' );
let all = document.querySelector("*");
all.onclick = ()=>game.scale.startFullScreen(false);
all.ontouchstart = ()=>game.scale.startFullScreen(false);
window["game"] = game;