const Config = {
    cameraSpeed: 1,
    bgSize: 805,
    doodleSpeed: 340,
    sprites: {},
    colors: {
        background: '#232323'
    },
    platform: {
        width: 50,//not in use
        height: 9,//not in use
        amount: 9,
        margin: 70
    },
    window: {
        width: 50,
        height: 50,
        scaleX: 0.9,
        scaleY: 0.9
    },
    text: {
        top: 5,
        left: 10
    },
    bottom: {
        width: 300,
        height: 41
    }
};
export default Config;