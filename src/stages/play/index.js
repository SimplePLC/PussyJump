import Config from "../../config";


let gyroE = {
  gamma: 0
}
window.addEventListener("deviceorientation", (e)=>gyroE = e, true);
export const Play = game => {
  return {
    preload: function() {
      // Sprites
      this.load.image("doodle", "./sprites/pussy.png");
      this.load.image(
        "pixel",
        "https://s3-us-west-2.amazonaws.com/s.cdpn.io/836/pixel_1.png"
      );
      this.load.image("platform", "./sprites/windowbank.png");
      this.load.image("window", "./sprites/window.png");
      this.load.image("bottom", "./sprites/bottom.png");
      this.load.image("bg", "./sprites/bg_1.png");

      // Sounds
      this.load.audio("jumpsound", "./sounds/jump.wav");
    },

    create: function() {
      // background color
      // this.stage.backgroundColor = Config.colors.background;
      this.paralax = game.add.tileSprite( 0,0,600,1000,"bg");
      this.paralax2 = game.add.tileSprite( 0,-1000,600,1000,"bg");

      // sounds
      this.jumpSound = game.add.audio("jumpsound");

      // text
      this.scoreText = game.add.text(Config.text.left, Config.text.top, "0");  
      this.scoreText.font = 'Arial';
      this.scoreText.fontSize = 20;
      this.scoreText.fill = '#ffffff';
      //game.sound.setDecodedCallback([this.jumpSound], start, this);

      // scaling
      this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
      this.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
      this.scale.setScreenSize(true);
      //this.scale.startFullScreen(true);

      // physics
      this.physics.startSystem(Phaser.Physics.ARCADE);

      // camera and platform tracking vars
      this.cameraYMin = 99999;
      this.platformYMin = 99999;
      this.score = 0;

      // create platforms
      this.platformsCreate();

      //var client = new Client(); create doodle
      this.doodleCreate();

      this.cursor = this.input.keyboard.createCursorKeys();
    },

    update: function() {
      // this is where the main magic happens
      // the y offset and the height of the world are adjusted
      // to match the highest point the doodle has reached
      this.world.setBounds(
        0,
        -this.doodle.yChange,
        this.world.width,
        this.game.height + this.doodle.yChange
      );

      

      // camera follows doodle
      this.cameraYMin = Math.min(
        this.cameraYMin,
        this.doodle.y - this.game.height + 130
      );
      this.camera.y = this.cameraYMin;

      this.score = Math.abs(this.camera.y);


      /*this.paralax.y = -1000 * (this.camera.y%2000);
      this.paralax2.y = -1000 /** (this.camera.y%1000)*/;

      const generation = parseInt(this.score/Config.bgSize);
      this.paralax.y = -Config.bgSize * generation;
      this.paralax2.y = -Config.bgSize * (generation+1);
      
      //console.log(this.camera.y);


      // doodle collisions and movement
      this.physics.arcade.collide(this.doodle, this.platforms);
      this.doodleMove();

      // for each platform, find out which is the highest
      // if one goes below the camera view, then create a new one at a distance from the highest one
      // these are pooled so they are very performant
      this.platforms.forEachAlive(function(elem) {
        this.platformYMin = Math.min(this.platformYMin, elem.y);
        const cameraY = this.camera.y;
        const gameHeight = this.game.height;
        if (elem.y > cameraY + gameHeight) {
          const x = this.rnd.integerInRange(0, this.world.width - 50);
          elem.kill();
          // Last parameter true ==> breakable (one time use)
          this.platformsCreateOne(x, this.platformYMin - 100, 50/*, (Math.random()>0.85)?true:false*/);
          let _this = this;
          // find matching window
          this.platformWindows.forEachAlive(function(winElem) {
            if (winElem.y > cameraY + gameHeight) {
              winElem.kill();
              _this.windowCreateOne(x, _this.platformYMin - 100, 50);
            }
          }, true);
        }
      }, this);


      // text
      this.scoreText.setText(this.score.toString());
      this.scoreText.y = this.camera.y + Config.text.top;
      
    },

    shutdown: function() {
      // reset everything, or the world will be messed up
      let name = prompt("Congratulation, you reached " + this.score + " points.", "Your Name");
      
      if(!(name==="Your Name" || name.length < 1)) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open( "GET", "https://lksfnd.space/projects/fatcat/addScore.php?name="+name+"&score="+this.score, false ); // false for synchronous request
        xmlHttp.send( null );
  
      }
      
      this.world.setBounds(0, 0, this.game.width, this.game.height);
      this.cursor = null;
      this.doodle.destroy();
      this.doodle = null;
      this.platforms.destroy();
      this.platforms = null;
    },

    platformsCreate: function() {
      // platform basic setup
      this.platforms = this.add.group();
      // windows for platforms
      this.platformWindows = this.add.group();

      
      this.platforms.enableBody = true;
      this.platformWindows.enableBody = true;
      this.platforms.createMultiple(10, "platform");
      this.platformWindows.createMultiple(10, "window");

      // create the base platform, with buffer on either side so that the doodle doesn't fall through
      this.platformCreateStart(
        -Config.platform.width,
        this.world.height - Config.platform.height,
        this.world.width + Config.platform.width
      );

      // create a batch of platforms that start to move up the level
      for (var i = 0; i < Config.platform.amount; i++) {
        const x = this.rnd.integerInRange(
          0,
          this.world.width - Config.platform.width
        ); // x position (random)
        this.platformsCreateOne(
          x,
          this.world.height -
            Config.platform.margin -
            Config.platform.margin * i, // y position
          1 // width (scale)
        );
        this.windowCreateOne(
          x,
          this.world.height -
            Config.platform.margin -
            Config.platform.margin * i,
          1
        );
      }
    },

    platformsCreateOne: function(x, y, width, breakable = false) {
      // this is a helper function since writing all of this out can get verbose elsewhere
      var platform = this.platforms.getFirstDead();
      platform.reset(x, y);
      platform.scale.x = 1;
      platform.scale.y = 1;
      platform.body.immovable = !breakable;
    },

    windowCreateOne: function(x, y, width) {
      var window = this.platformWindows.getFirstDead();
      window.reset(
        x + 5,
        y -
          Config.window.height +
          (1 - Config.window.scaleY) * Config.window.height
      );
      window.scale.x = Config.window.scaleX;
      window.scale.y = Config.window.scaleY;
      return window;
    },

    /* Creates the platform on the bottom */
    platformCreateStart: function(x, y, width) {
      var platform = this.platforms.getFirstDead();
      platform.reset(x, y);
      platform.scale.x = this.world.width / Config.platform.width + 2;
      platform.scale.y = 1;
      platform.body.immovable = true;
      return platform;
    },

    doodleCreate: function() {
      // basic doodle setup
      this.doodle = game.add.sprite(
        this.world.centerX,
        this.world.height - 36,
        "doodle"
      );
      this.doodle.anchor.set(0.5);

      // track where the doodle started and how much the distance has changed from that point
      this.doodle.yOrig = this.doodle.y;
      this.doodle.yChange = 0;

      // doodle collision setup
      // disable all collisions except for down
      this.physics.arcade.enable(this.doodle);
      this.doodle.body.gravity.y = 550;
      this.doodle.body.checkCollision.up = false;
      this.doodle.body.checkCollision.left = false;
      this.doodle.body.checkCollision.right = false;
    },

    doodleMove: function() {
      // handle the left and right movement of the doodle
      let gamma = gyroE.gamma;
        let speed = 0;
        let dir = 1;
        if(gamma < 0){
          gamma = 360+gamma;
        }
        if(gamma > 0 && gamma < 180){
          gamma = (gamma > 90)?90:gamma;
          speed = gamma;
        }else if(gamma > 180){
          gamma = (gamma < 270)?270:gamma;
          dir = -1;
          speed = 360-gamma;
        }
        this.doodle.body.velocity.x = speed*10*dir;
      if (this.cursor.left.isDown) {
        this.doodle.body.velocity.x = -200;
      } else if (this.cursor.right.isDown) {
        this.doodle.body.velocity.x = 200;
      }
      
      // handle doodle jumping
      if (this.doodle.body.touching.down) {
        this.doodle.body.velocity.y = -Config.doodleSpeed;
        this.jumpSound.play();
      }

      // wrap world coordinated so that you can warp from left to right and right to left
      this.world.wrap(this.doodle, this.doodle.width / 2, false);

      // track the maximum amount that the doodle has travelled
      this.doodle.yChange = Math.max(
        this.doodle.yChange,
        Math.abs(this.doodle.y - this.doodle.yOrig)
      );

      // if the doodle falls below the camera view, gameover
      if (
        this.doodle.y > this.cameraYMin + this.game.height &&
        this.doodle.alive
      ) {
        this.state.start("Play");
      }
    }
  };
};
export default Play;
