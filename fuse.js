const { src, task, exec, context } = require("fuse-box/sparky");
const {
  FuseBox,
  BabelPlugin,
  WebIndexPlugin,
  QuantumPlugin,
  ReplacePlugin
} = require("fuse-box");

const fuse = context(
  class {
    getConfig() {
      return FuseBox.init({
        homeDir: "src",
        target: "browser@es6",
        output: "build/$name.js",
        sourceMaps: true,
    	shim: {
	        phaser: {
	            source: "./phaser.min.js",
	            exports: "Phaser",
	        }
   		},
        plugins: [
          WebIndexPlugin({
            template: "src/index.html",
            appendBundles: true
          }),
          ReplacePlugin({
          	"WEBGL_RENDERER": true,
          	"CANVAS_RENDERER": true
          }),
          // this.isProduction && BabelPlugin(),
          this.isProduction &&
            QuantumPlugin({
              uglify: true,
              treeshake: true,
            }),
        ],
      });
    }
  }
);
task("default", async context => {
  const fuse = context.getConfig();
  fuse.dev();
  fuse
    .bundle("app")
    .hmr()
    .watch()
    .instructions(" > index.js");
  await fuse.run();
});
task("prod", async context => {
  context.isProduction = true;
  const fuse = context.getConfig();
  fuse.bundle("app").instructions(" > index.js");
  await fuse.run();
});
task("build", async context => {
  const fuse = context.getConfig();
  fuse.bundle("app").instructions(" > index.js");
  await fuse.run();
})
task("prod:web", async context => {
  context.isProduction = true;
  const fuse = context.getConfig();
  fuse.dev();
  fuse.bundle("app").instructions(" > app.js");
  await fuse.run();
});
